package com.soorya.remoteresponse.logic;

import android.util.Log;

public class Trigger {
    public static String createTriggerString(String deviceName, String password, String commandCode) {
        String trigger = deviceName + "(" + password + "):" + commandCode;
        Log.d("TpRemoteDeviceFragment", "Generated trigger string: " + trigger);
        return trigger;
    }
}
