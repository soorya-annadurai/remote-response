package com.soorya.remoteresponse.logic;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.soorya.remoteresponse.R;
import com.soorya.remoteresponse.logic.ProfileWrapper.ProfileWrapper;

import java.util.List;

public class ProfileWrapperAdapter extends RecyclerView.Adapter<ProfileWrapperAdapter.MyViewHolder> {
    private List<ProfileWrapper> profileWrapperList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
        }
    }

    public ProfileWrapperAdapter(List<ProfileWrapper> profileWrapperList) {
        this.profileWrapperList = profileWrapperList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.profile_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProfileWrapper profileWrapper = profileWrapperList.get(position);
        holder.title.setText(profileWrapper.getProfile().getNickname());
        holder.genre.setText(profileWrapper.getProfile().getDeviceName());
        holder.year.setText(profileWrapper.getProfile().getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return profileWrapperList.size();
    }
}
