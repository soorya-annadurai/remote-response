package com.soorya.remoteresponse.logic.ProfileWrapper.Profile;

public class Profile {
    private String nickname;
    private String deviceName;
    private String devicePassword;
    private String phoneNumber;

    public String getNickname() {
        return nickname;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDevicePassword() {
        return devicePassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Profile (String nickname, String deviceName, String devicePassword, String phoneNumber) {
        this.nickname = nickname;
        this.deviceName = deviceName;
        this.devicePassword = devicePassword;
        this.phoneNumber = phoneNumber;
    }
}
