package com.soorya.remoteresponse.logic.ProfileWrapper;

import com.soorya.remoteresponse.logic.ProfileWrapper.Profile.Profile;

public class ProfileWrapper {
    private Profile profile;
    private Integer usageCount;
    private boolean isFavorite;
    private Integer displayPosition;

    public Profile getProfile() {
        return profile;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public Integer getUsageCount() {
        return usageCount;
    }

    public Integer getDisplayPosition() {
        return displayPosition;
    }

    public void setDisplayPosition(Integer displayPosition) {
        this.displayPosition = displayPosition;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setUsageCount(Integer usageCount) {
        this.usageCount = usageCount;
    }

    public ProfileWrapper(String nickname, String deviceName, String devicePassword, String phoneNumber, Integer usageCount, boolean isFavorite, Integer displayPosition) {
        setProfile(new Profile(nickname, deviceName, devicePassword, phoneNumber));
        setUsageCount(usageCount);
        setFavorite(isFavorite);
        setDisplayPosition(displayPosition);
    }

    public ProfileWrapper(Profile profile, Integer usageCount, boolean isFavorite, Integer displayPosition) {
        setProfile(profile);
        setUsageCount(usageCount);
        setFavorite(isFavorite);
        setDisplayPosition(displayPosition);
    }
}
