package com.soorya.remoteresponse;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.soorya.remoteresponse.BasePoint.ConfigureResponses.BpConfigureResponsesFragment;
import com.soorya.remoteresponse.BasePoint.ManageMyProfile.BpManageMyProfileFragment;
import com.soorya.remoteresponse.TriggerPoint.ManageRemoteProfiles.TpManageRemoteProfilesFragment;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.TpTriggerRemoteDeviceFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
//            Choosing the fragment to trigger a remote device by default.
            navigationView.getMenu().findItem(R.id.nav_tp_trigger_remote_device).setChecked(true);
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_tp_trigger_remote_device));
        }

//        Request for relevant permissions
//        TODO: Ask for permissions only after end of "tutorial".
        String[] requiredPermissions = {
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.SEND_SMS,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.SET_ALARM,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                Manifest.permission.READ_CONTACTS
        };
        Integer requestCode = 1;
//        TODO: Post an alert or a snackbar re-requesting for permissions.
        checkAndRequestPermissions(requiredPermissions, requestCode);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.

        Fragment fragment = null;

        int id = item.getItemId();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().findItem(R.id.nav_tp_trigger_remote_device).setChecked(false);
        navigationView.getMenu().findItem(R.id.nav_tp_manage_remote_profiles).setChecked(false);
        navigationView.getMenu().findItem(R.id.nav_bp_configure_responses).setChecked(false);
        navigationView.getMenu().findItem(R.id.nav_bp_manage_my_profile).setChecked(false);

        if (id == R.id.nav_tp_trigger_remote_device) {
            fragment = new TpTriggerRemoteDeviceFragment();
            navigationView.getMenu().findItem(R.id.nav_tp_trigger_remote_device).setChecked(true);
        } else if (id == R.id.nav_tp_manage_remote_profiles) {
            fragment = new TpManageRemoteProfilesFragment();
            navigationView.getMenu().findItem(R.id.nav_tp_manage_remote_profiles).setChecked(true);
        } else if (id == R.id.nav_bp_configure_responses) {
            fragment = new BpConfigureResponsesFragment();
            navigationView.getMenu().findItem(R.id.nav_bp_configure_responses).setChecked(true);
        } else if (id == R.id.nav_bp_manage_my_profile) {
            fragment = new BpManageMyProfileFragment();
            navigationView.getMenu().findItem(R.id.nav_bp_manage_my_profile).setChecked(true);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.screen_area, fragment, fragment.getClass().toString());
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkAndRequestPermissions(String[] permissions, Integer requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permission);
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), requestCode);
            }
        }
    }
}
