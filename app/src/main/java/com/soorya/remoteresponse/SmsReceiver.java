package com.soorya.remoteresponse;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (intent.getAction()
                .equals("android.provider.Telephony.SMS_RECEIVED")) {
            Toast toast = Toast.makeText(context, "message received",
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
