package com.soorya.remoteresponse.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import permissions.dispatcher.PermissionRequest;

public class PermissionRequestAlertDialogFragment extends DialogFragment {
    PermissionRequest request;

    public PermissionRequestAlertDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    public static PermissionRequestAlertDialogFragment newInstance(String title, String message, @NonNull PermissionRequest request) {
        PermissionRequestAlertDialogFragment frag = new PermissionRequestAlertDialogFragment();
        frag.setRequest(request);
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }

    public void setRequest(PermissionRequest request) {
        this.request = request;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                request.proceed();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                request.cancel();
//                if (dialog != null && dialog.isShowing()) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

        });
        return alertDialogBuilder.create();
    }
}