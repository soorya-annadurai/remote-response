package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs.RemoteContact;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.android.ex.chips.recipientchip.DrawableRecipientChip;

public class RemoteContactUtilities {
    public static void configureRemoteContactInput(
            @NonNull final Fragment fragment,
            @NonNull final RecipientEditTextView recipientEditTextView,
            @NonNull final ImageButton buttonLaunchContactPicker,
            final int RESULT_PICK_CONTACT) {
        recipientEditTextView.enableDrag();
        recipientEditTextView.performValidation();
        recipientEditTextView.setMaxChips(1);

        if (fragment.getContext() != null) {
            recipientEditTextView.setChipNotCreatedListener(new RecipientEditTextView.ChipNotCreatedListener() {
                @Override
                public void chipNotCreated(String chipText) {
                    cleanAndTransformRecipientEditTextViewContentsToChip(recipientEditTextView);
                    Toast.makeText(fragment.getContext(), "Only one remote contact is allowed. Chip not created for: " + chipText, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if (fragment.getContext() != null) {
            recipientEditTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            BaseRecipientAdapter adapter = new BaseRecipientAdapter(BaseRecipientAdapter.QUERY_TYPE_PHONE, fragment.getContext());

            adapter.setShowMobileOnly(false);
            recipientEditTextView.setAdapter(adapter);
        }

        recipientEditTextView.dismissDropDownOnItemSelected(true);


        buttonLaunchContactPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                fragment.startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
            }
        });

    }

    private static void cleanAndTransformRecipientEditTextViewContentsToChip(@NonNull RecipientEditTextView recipientEditTextView) {
        DrawableRecipientChip[] chips = recipientEditTextView.getSortedRecipients();
        if (chips.length == 0) {
            String text = recipientEditTextView.getText().toString();
            text = text.replaceAll("\\s", "");
            String split[] = text.split(",", 2); // will be matched 1 times.
            text = split[0];  //before the first comma. `abc`
            recipientEditTextView.setText("");
            recipientEditTextView.submitItem(text, text);
        }
    }
}
