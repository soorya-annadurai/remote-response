package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ex.chips.RecipientEditTextView;
import com.soorya.remoteresponse.R;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Buttons.ButtonUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs.InputUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.SlidingNotice.SlidingNoticeUtilities;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import fr.ganfra.materialspinner.MaterialSpinner;

public class SendTriggerUtilities {
    public static void configureSendTriggerPanel(
            @NonNull final Fragment fragment,
            @NonNull final SlidingUpPanelLayout panelLayout,
            @NonNull final ImageView imageViewForViewingProfiles,
            @NonNull final TextView textViewForViewingProfiles,
            @NonNull final SlidingUpPanelLayout addProfilePanelLayout,
            @NonNull final FloatingActionButton floatingActionButton,
            @NonNull final MaterialSpinner spinnerCommand,
            @NonNull final EditText editTextRemoteDeviceName,
            @NonNull final RecipientEditTextView retvContact,
            @NonNull final ImageButton buttonLaunchContactPicker,
            @NonNull final Button buttonOpenSmsApplication,
            final int RESULT_PICK_CONTACT) {
        panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        panelLayout.setFocusable(true);
        panelLayout.setClickable(true);
        panelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });
        panelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                imageViewForViewingProfiles.setRotation(slideOffset * 180);
                if (slideOffset < 0.5) {
                    textViewForViewingProfiles.setText(R.string.notice_slide_up);
                } else {
                    textViewForViewingProfiles.setText(R.string.notice_slide_down);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    imageViewForViewingProfiles.setRotation(180);
                    textViewForViewingProfiles.setText(R.string.notice_slide_down);
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    imageViewForViewingProfiles.setRotation(0);
                    textViewForViewingProfiles.setText(R.string.notice_slide_up);
                } else if (previousState.equals(SlidingUpPanelLayout.PanelState.DRAGGING) &&
                        newState.equals(SlidingUpPanelLayout.PanelState.ANCHORED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }

                if (newState.equals(SlidingUpPanelLayout.PanelState.DRAGGING) || newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    addProfilePanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }

                if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED && addProfilePanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    floatingActionButton.show();
                } else {
                    floatingActionButton.hide();
                }
                if (fragment.getActivity() != null) {
                    fragment.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

                Log.i("TriggerRemote", "Changed the panel state from " + previousState.toString() + " to " + newState.toString());
            }
        });
        panelLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    textViewForViewingProfiles.setText(R.string.notice_slide_down);
                    imageViewForViewingProfiles.setRotation(180);
                } else if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    textViewForViewingProfiles.setText(R.string.notice_slide_up);
                    imageViewForViewingProfiles.setRotation(0);
                }
//                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        SlidingNoticeUtilities.configureSlidingNotice(imageViewForViewingProfiles, textViewForViewingProfiles, panelLayout);
        InputUtilities.configureInputs(fragment, spinnerCommand, editTextRemoteDeviceName, retvContact, buttonLaunchContactPicker, RESULT_PICK_CONTACT);
        ButtonUtilities.configureButtons(fragment, buttonOpenSmsApplication);
    }
}
