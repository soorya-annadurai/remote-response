package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.ViewProfiles;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.soorya.remoteresponse.logic.ProfileWrapper.ProfileWrapper;
import com.soorya.remoteresponse.logic.ProfileWrapperAdapter;

import java.util.List;

public class ViewProfilesUtilities {
    public static void configureViewProfilesLayout(
            @NonNull final Fragment fragment,
            @NonNull final RecyclerView recyclerView,
            @NonNull final List<ProfileWrapper> profileWrapperList
    ) {
        recyclerView.setLayoutManager(new LinearLayoutManager(fragment.getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new ProfileWrapperAdapter(profileWrapperList));
    }
}
