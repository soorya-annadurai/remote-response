package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Buttons;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Button;

import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Buttons.OpenSmsApplication.OpenSmsApplicationUtilities;

public class ButtonUtilities {
    public static void configureButtons(
            @NonNull final Fragment fragment,
            @NonNull final Button openSmsApplicationButton
    ) {
        OpenSmsApplicationUtilities.configureOpenSmsApplicationButton(fragment, openSmsApplicationButton);
    }
}
