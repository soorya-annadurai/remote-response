package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs.CommandType;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.soorya.remoteresponse.R;

import fr.ganfra.materialspinner.MaterialSpinner;

public class CommandTypeUtilities {
    public static void configureCommandTypeInput(
            @NonNull final Fragment fragment,
            @NonNull final MaterialSpinner spinner,
            final int hintColor,
            final float textSize) {
        spinner.setHintColor(hintColor);
        spinner.setHintTextSize(textSize);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0) {
                    String command_list_descriptions[] = fragment.getResources().getStringArray(R.array.command_list_description);
                    Toast.makeText(fragment.getContext(), command_list_descriptions[i], Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }
}
