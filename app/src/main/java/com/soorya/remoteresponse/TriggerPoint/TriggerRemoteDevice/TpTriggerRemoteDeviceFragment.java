package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ex.chips.RecipientEditTextView;
import com.android.ex.chips.recipientchip.DrawableRecipientChip;
import com.soorya.remoteresponse.R;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.AddProfile.AddProfileUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.Miscellaneous.MiscellaneousUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.SendTriggerUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.ViewProfiles.ViewProfilesUtilities;
import com.soorya.remoteresponse.logic.ProfileWrapper.ProfileWrapper;
import com.soorya.remoteresponse.logic.ProfileWrapperAdapter;
import com.soorya.remoteresponse.logic.Trigger;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

@RuntimePermissions
public class TpTriggerRemoteDeviceFragment extends Fragment {

    private static final int RESULT_PICK_CONTACT = 8550;
    /*
    //    Declaration for visual elements
    */
//    Button to toggle display of the sliding panel to add a profile
    FloatingActionButton floatingActionButton = null;
    //    Sliding-up panel to send a trigger
    SlidingUpPanelLayout slidingUpPanelLayout_sendTrigger = null;
    //    Components in panel to send a trigger
    EditText editTextRemoteDeviceName_sendTrigger = null;
    EditText editTextRemoteDevicePassword_sendTrigger = null;
    MaterialSpinner spinnerCommandType_sendTrigger = null;
    RecipientEditTextView retvContact_sendTrigger = null;
    Button buttonSendTrigger_sendTrigger = null;
    Button buttonOpenSmsApplication_sendTrigger = null;
    ImageButton buttonLaunchContactPicker_sendTrigger = null;
    ImageView imageViewNoticeForViewingProfiles_sendTrigger = null;
    TextView textViewNoticeForViewingProfiles_sendTrigger = null;

    //    Sliding-down panel for adding profiles
    SlidingUpPanelLayout slidingDownPanelLayout_addProfile = null;

    //    Components for displaying list of saved profiles
    List<ProfileWrapper> profileWrapperList = null;
    RecyclerView recyclerView = null;
    ProfileWrapperAdapter profileWrapperAdapter = null;

    //    Global variables for sending an SMS
    private String triggerString = null;
    private String remotePhoneNumber = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tp_trigger_remote_device, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        Initialize all components
        floatingActionButton = view.findViewById(R.id.fab);
        slidingUpPanelLayout_sendTrigger = view.findViewById(R.id.slidingLayout_sendTrigger);
        editTextRemoteDeviceName_sendTrigger = view.findViewById(R.id.editText_remoteDeviceName_sendTrigger);
        editTextRemoteDevicePassword_sendTrigger = view.findViewById(R.id.editText_remoteDevicePassword_sendTrigger);
        buttonLaunchContactPicker_sendTrigger = view.findViewById(R.id.imageButton_showAllContacts_sendTrigger);
        retvContact_sendTrigger = view.findViewById(R.id.retv_contact_sendTrigger);
        spinnerCommandType_sendTrigger = view.findViewById(R.id.spinner_commandType_sendTrigger);
        buttonSendTrigger_sendTrigger = view.findViewById(R.id.button_sendTrigger_sendTrigger);
        buttonOpenSmsApplication_sendTrigger = view.findViewById(R.id.button_openSmsApplication_sendTrigger);
        imageViewNoticeForViewingProfiles_sendTrigger = view.findViewById(R.id.imageView_notice_SendTrigger);
        textViewNoticeForViewingProfiles_sendTrigger = view.findViewById(R.id.textView_notice_sendTrigger);
        slidingDownPanelLayout_addProfile = view.findViewById(R.id.slidingLayout_addProfile);
        recyclerView = view.findViewById(R.id.recyclerView_viewProfiles);
        profileWrapperList = new ArrayList<>();
        profileWrapperAdapter = new ProfileWrapperAdapter(profileWrapperList);

//        Configure all components and their children
        SendTriggerUtilities.configureSendTriggerPanel(this, slidingUpPanelLayout_sendTrigger, imageViewNoticeForViewingProfiles_sendTrigger, textViewNoticeForViewingProfiles_sendTrigger, slidingDownPanelLayout_addProfile, floatingActionButton, spinnerCommandType_sendTrigger, editTextRemoteDeviceName_sendTrigger, retvContact_sendTrigger, buttonLaunchContactPicker_sendTrigger, buttonOpenSmsApplication_sendTrigger, RESULT_PICK_CONTACT);
        AddProfileUtilities.configureAddProfilePanel(this, slidingDownPanelLayout_addProfile, slidingUpPanelLayout_sendTrigger, floatingActionButton);
        ViewProfilesUtilities.configureViewProfilesLayout(this, recyclerView, profileWrapperList);
        MiscellaneousUtilities.configureMiscellaneousComponents(floatingActionButton, slidingUpPanelLayout_sendTrigger, slidingDownPanelLayout_addProfile);
//        TODO: Move this to inner class
        retvContact_sendTrigger.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    TpTriggerRemoteDeviceFragmentPermissionsDispatcher.stubRequestReadContactsPermissionWithPermissionCheck(TpTriggerRemoteDeviceFragment.this);
                }
                cleanAndTransformRecipientEditTextViewContentsToChip();
            }
        });

//        TODO: Move this to inner class
        buttonSendTrigger_sendTrigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int commandIndex = spinnerCommandType_sendTrigger.getSelectedItemPosition();
                if (spinnerCommandType_sendTrigger.getHint() != null) {
                    commandIndex -= 1;
                }
                String deviceName = editTextRemoteDeviceName_sendTrigger.getText().toString();
                String password = editTextRemoteDevicePassword_sendTrigger.getText().toString();

                cleanAndTransformRecipientEditTextViewContentsToChip();
                DrawableRecipientChip[] chips = retvContact_sendTrigger.getSortedRecipients();
                for (DrawableRecipientChip chip : chips) {
                    Log.i("DrawableChip", chip.getEntry().getDisplayName() + ":" + chip.getEntry().getDestination());
                }
                if (deviceName.isEmpty() || password.isEmpty() || chips.length == 0 || commandIndex == -1) {
                    Toast.makeText(v.getContext(), "All fields need to be filled!", Toast.LENGTH_LONG).show();
                } else {
                    remotePhoneNumber = chips[0].getEntry().getDestination();
                    String commandListCode[] = getResources().getStringArray(R.array.command_list_code);
                    String commandCode = commandListCode[commandIndex];
                    triggerString = Trigger.createTriggerString(deviceName, password, commandCode);
                    TpTriggerRemoteDeviceFragmentPermissionsDispatcher.sendSmsMsgFncWithPermissionCheck(TpTriggerRemoteDeviceFragment.this, remotePhoneNumber, triggerString);
                }
            }
        });

        prepareProfileWrapperSampleData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be using multiple startActivityForResult
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Toast.makeText(getContext(), "Did not pick a contact!", Toast.LENGTH_SHORT).show();
        }
    }

    @NeedsPermission(Manifest.permission.SEND_SMS)
    public void sendSmsMsgFnc(String mblNumVar, String smsMsgVar) {
        if (smsMsgVar != null && !smsMsgVar.isEmpty() && mblNumVar != null && !mblNumVar.isEmpty()) {
            try {
                SmsManager smsMgrVar = SmsManager.getDefault();
                smsMgrVar.sendTextMessage(mblNumVar, null, smsMsgVar, null, null);
//                    TODO: Check if message is actually sent and delivered. Possibly, add history and/or queue of triggers.
                Toast.makeText(getContext(), "Message Sent", Toast.LENGTH_LONG).show();
            } catch (Exception ErrVar) {
                Toast.makeText(getContext(), ErrVar.getMessage(), Toast.LENGTH_LONG).show();
                ErrVar.printStackTrace();
            }
        }
    }

    @NeedsPermission(Manifest.permission.READ_CONTACTS)
    public void stubRequestReadContactsPermission() {
    }

    public void cleanAndTransformRecipientEditTextViewContentsToChip() {
        if (retvContact_sendTrigger != null) {
            DrawableRecipientChip[] chips = retvContact_sendTrigger.getSortedRecipients();
            if (chips.length == 0) {
                String text = retvContact_sendTrigger.getText().toString();
                text = text.replaceAll("\\s", "");
                String split[] = text.split(",", 2); // will be matched 1 times.
                text = split[0];  //before the first comma. `abc`
                retvContact_sendTrigger.setText("");
                retvContact_sendTrigger.submitItem(text, text);
            }
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null;
            String name = null;
            String photoThumbnailUriString = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            if (getActivity() != null && uri != null) {
                cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            }
            if (cursor != null) {
                cursor.moveToFirst();

                // column index of the phone number
                int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                // column index of the contact name
                int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                // column index of the contact thumbnail URI
                int photoThumbnailUriIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);

                phoneNo = cursor.getString(phoneIndex);
                name = cursor.getString(nameIndex);
                photoThumbnailUriString = cursor.getString(photoThumbnailUriIndex);
                cursor.close();
            }
            Uri photoThumbnailUri = null;
            try {
                photoThumbnailUri = Uri.parse(photoThumbnailUriString);
            } catch (NullPointerException ignored) {
            }
            retvContact_sendTrigger.setText("");
            try {
                retvContact_sendTrigger.submitItem(name, phoneNo, photoThumbnailUri);
            } catch (SecurityException exc) {
                retvContact_sendTrigger.submitItem(name, phoneNo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Annotate a method which explains why the permission/s is/are needed.
    // It passes in a `PermissionRequest` object which can continue or abort the current permission
    @OnShowRationale(Manifest.permission.SEND_SMS)
    void showRationaleForSendSms(final PermissionRequest request) {
//        TODO: Use a DialogFragment to persist the alert after screen rotation
//        PermissionRequestAlertDialogFragment fragment = PermissionRequestAlertDialogFragment.newInstance("Permission to send SMS", "This application needs permission to send SMS, in order to trigger a remote device.", request);
//        fragment.show(getActivity().getSupportFragmentManager(), "perm_req_dialog");
        if (getContext() != null) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Permission to send SMS")
                    .setMessage("This application needs permission to send SMS, in order to trigger a remote device.")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            request.proceed();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            request.cancel();
                        }
                    })
                    .show();
        }
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.SEND_SMS)
    void showDeniedForSendSms() {
        if (getView() != null) {
            Snackbar
                    .make(getView(), "Permission to send SMS was denied! The remote device could not be triggered.", Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    // Annotates a method which is invoked if the user chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.SEND_SMS)
    void showNeverAskForSendSms() {
        if (getView() != null && getActivity() != null && getActivity().getPackageName() != null) {
            Snackbar
                    .make(getView(), "Permission to send SMS is needed! Grant it in the app settings.", Snackbar.LENGTH_LONG)
                    .setAction("Open Settings", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                    .show();
        }
    }

    @OnShowRationale(Manifest.permission.READ_CONTACTS)
    void showRationaleForReadContacts(final PermissionRequest request) {
//        TODO: Use a DialogFragment to persist the alert after screen rotation
//        PermissionRequestAlertDialogFragment fragment = PermissionRequestAlertDialogFragment.newInstance("Permission to read contacts", "This application needs permission to read your contacts, in order to show contact suggestions.", request);
//        fragment.show(getActivity().getSupportFragmentManager(), "perm_req_dialog");
        if (getContext() != null) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Permission to read contacts")
                    .setMessage("This application needs permission to read your contacts, in order to show contact suggestions.")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            request.proceed();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            request.cancel();
                        }
                    })
                    .show();
        }
    }

    @OnNeverAskAgain(Manifest.permission.READ_CONTACTS)
    void showNeverAskForReadContacts() {
        if (getView() != null && getActivity() != null && getActivity().getPackageName() != null) {
            Snackbar
                    .make(getView(), "Want suggestions? Give permission to read contacts in settings.", Snackbar.LENGTH_LONG)
                    .setAction("Open Settings", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        TpTriggerRemoteDeviceFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    private void prepareProfileWrapperSampleData() {
        ProfileWrapper profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Soorya's OnePlus", "luna", "parvathi", "+919632293466", 0, false, 0
        );
        profileWrapperList.add(profileWrapper);

        profileWrapper = new ProfileWrapper(
                "Parvathi's Pixel", "maluspixel", "soorya", "+918050201928", 0, false, 1
        );
        profileWrapperList.add(profileWrapper);

        profileWrapperAdapter.notifyDataSetChanged();
    }
}
