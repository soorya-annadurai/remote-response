package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.ex.chips.RecipientEditTextView;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs.CommandType.CommandTypeUtilities;
import com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Inputs.RemoteContact.RemoteContactUtilities;

import fr.ganfra.materialspinner.MaterialSpinner;

public class InputUtilities {
    public static void configureInputs(
            @NonNull final Fragment fragment,
            @NonNull final MaterialSpinner spinnerCommand,
            @NonNull final EditText editTextRemoteDeviceName,
            @NonNull final RecipientEditTextView recipientEditTextView,
            @NonNull final ImageButton buttonLaunchContactPicker,
            final int RESULT_PICK_CONTACT) {
//        TODO: Click outside an EditView to lose its focus
        RemoteContactUtilities.configureRemoteContactInput(fragment, recipientEditTextView, buttonLaunchContactPicker, RESULT_PICK_CONTACT);
        CommandTypeUtilities.configureCommandTypeInput(fragment, spinnerCommand, editTextRemoteDeviceName.getCurrentHintTextColor(), editTextRemoteDeviceName.getTextSize());
    }
}
