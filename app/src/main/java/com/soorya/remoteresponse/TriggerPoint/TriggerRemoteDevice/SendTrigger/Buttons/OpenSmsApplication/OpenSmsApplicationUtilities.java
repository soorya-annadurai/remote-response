package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.Buttons.OpenSmsApplication;

import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

public class OpenSmsApplicationUtilities {
    public static void configureOpenSmsApplicationButton(
            @NonNull final Fragment fragment,
            @NonNull final Button button
    ) {
        if (fragment.getActivity() != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String defaultSmsPackage = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                            ? Telephony.Sms.getDefaultSmsPackage(fragment.getContext())
                            : Settings.Secure.getString(fragment.getActivity().getContentResolver(), "sms_default_application");

                    Intent smsIntent = fragment.getActivity().getPackageManager().getLaunchIntentForPackage(defaultSmsPackage);
                    if (smsIntent == null) {
                        smsIntent = new Intent(Intent.ACTION_MAIN);
                        smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                    }
                    try {
                        fragment.getActivity().startActivity(smsIntent);
                    } catch (Exception e) {
                        Log.w(TAG, "Could not open SMS application.", e);
                        // Inform user
                        Toast.makeText(
                                fragment.getContext(),
                                "Your SMS app could not be opened. Please open it manually.",
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
            });
        }
    }
}
