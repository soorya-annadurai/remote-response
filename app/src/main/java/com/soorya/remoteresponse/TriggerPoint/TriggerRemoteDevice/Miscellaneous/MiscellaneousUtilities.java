package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.Miscellaneous;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class MiscellaneousUtilities {
    public static void configureMiscellaneousComponents(
            @NonNull final FloatingActionButton floatingActionButton,
            @NonNull final SlidingUpPanelLayout sendTriggerPanelLayout,
            @NonNull final SlidingUpPanelLayout addProfilePanelLayout
    ) {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionButton.hide();
                addProfilePanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        floatingActionButton.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                if (sendTriggerPanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED) && addProfilePanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    floatingActionButton.show();
                } else {
                    floatingActionButton.hide();
                }
            }
        });
    }
}
