package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.AddProfile;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class AddProfileUtilities {
    public static void configureAddProfilePanel(
            @NonNull final Fragment fragment,
            @NonNull final SlidingUpPanelLayout panelLayout,
            @NonNull final SlidingUpPanelLayout sendTriggerPanelLayout,
            @NonNull final FloatingActionButton floatingActionButton
    ) {
        panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        panelLayout.setFocusable(true);
        panelLayout.setClickable(true);
        panelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (panelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });
        panelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
//                imageViewForViewingProfiles.setRotation(slideOffset * 180);
//                if (slideOffset < 0.5) {
//                    textViewForViewingProfiles.setText(R.string.notice_slide_up);
//                } else {
//                    textViewForViewingProfiles.setText(R.string.notice_slide_down);
//                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED && sendTriggerPanelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    floatingActionButton.show();
                } else {
                    floatingActionButton.hide();
                }
                if (fragment.getActivity() != null) {
                    fragment.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                }

                Log.i("TriggerRemote", "Changed the sliding down panel state from " + previousState.toString() + " to " + newState.toString());
            }
        });
        panelLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//                    TODO: Use the correct fields for the sliding notice
//                    textViewForViewingProfiles.setText(R.string.notice_slide_down);
//                    imageViewForViewingProfiles.setRotation(180);
                } else if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                    TODO: Use the correct fields for the sliding notice
//                    textViewForViewingProfiles.setText(R.string.notice_slide_up);
//                    imageViewForViewingProfiles.setRotation(0);
                }
            }
        });
    }
}
