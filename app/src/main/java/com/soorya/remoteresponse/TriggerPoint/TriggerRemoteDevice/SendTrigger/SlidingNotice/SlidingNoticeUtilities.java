package com.soorya.remoteresponse.TriggerPoint.TriggerRemoteDevice.SendTrigger.SlidingNotice;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.soorya.remoteresponse.R;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

public class SlidingNoticeUtilities {
    public static void configureSlidingNotice(
            @NonNull final ImageView imageViewForViewingProfiles,
            @NonNull final TextView textViewForViewingProfiles,
            @NonNull final SlidingUpPanelLayout panelLayout
    ) {
        View.OnClickListener triggerSlideOnNoticeClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.COLLAPSED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    panelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        };
        imageViewForViewingProfiles.setClickable(true);
        imageViewForViewingProfiles.setFocusable(true);
        imageViewForViewingProfiles.setFocusableInTouchMode(true);
        imageViewForViewingProfiles.setOnClickListener(triggerSlideOnNoticeClick);
        textViewForViewingProfiles.setClickable(true);
        textViewForViewingProfiles.setFocusable(true);
        textViewForViewingProfiles.setFocusableInTouchMode(true);
        textViewForViewingProfiles.setOnClickListener(triggerSlideOnNoticeClick);

        if (panelLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
            textViewForViewingProfiles.setText(R.string.notice_slide_down);
            imageViewForViewingProfiles.setRotation(180);
        } else {
            textViewForViewingProfiles.setText(R.string.notice_slide_up);
            imageViewForViewingProfiles.setRotation(0);
        }

//        TODO: If there are saved profiles, animate the arrow with the accent color
//        ColorAnimations.animateImageView(imageViewForViewingProfiles, getResources().getColor(R.color.colorAccent));

    }
}
